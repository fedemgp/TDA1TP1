close all;
clear all;
clc;
csv = dlmread('tiempos.txt',',');
figure
hold on;
xlabel('Y')
ylabel('Tiempo de ejecución[ms]')
title('Gale-Shapley para 1000 bandas y 1000 conciertos')
plot(csv(:,1),csv(:,2));