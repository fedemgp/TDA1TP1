# Trabajo practico de teoria de algoritmos 1

Federico Manuel Gomez Peter

Martín Cruz García

Lucas Etcheverry

## Compilación
Para compilar el proyecto se debe realizar lo siguiente.

- Crear una carpeta build en la raiz del repositorio
- Ejecutar las siguientes lineas en una consola (ubicado en la carpeta build)
    
    ```shell
    foo@bar:~/tp/build$ cmake ..
    foo@bar:~/tp/build$ make
    ```
## Ejecución
Primero se deben generar una serie de archivos para las bandas y los conciertos

  ```shell
  ./fileGen <N> <M>
  ```
Luego se ejecuta el algoritmo

  ```shell
  ./galeShapley <N> <M> <X> <Y>
  ```
Si se quiere ver solo el tiempo de ejecución, se debe cambiar el flag definido en 
el archivo CMakeLists.txt

- Si se quiere ver el resultado, setear el flag DEBUG
- Si solo quiere verse el tiempo de ejecución, setear flag PRINT_ONLY_TIME
- Si no se quiere imprimir nada en el estándar de salida, eliminar ambos flags

## Code style
Para revisar el coding style ejecutar el script `execute.sh`. Este linter recorrerá todos
los fuentes del directorio y analizará ciertos estilos de codificación, marcando 
aquellos que no cumplan el estandar del linter de google (se agregó una excepción 
para que no revise errores de tipo copyright).

Para mayor información revisar [la pagina de Google](https://google.github.io/styleguide/cppguide.html)