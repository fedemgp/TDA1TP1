#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" Description
This module implements an efficient version of the Erastotenes of 
Cirene algorithm  to calculate prime numbers smaller than a certain 
number N greater than 2.
Returns a list of prime numbers from 2 to "N" and the total execution 
time.

Example:
    $ python3 main.py 1000 F
    
Todo:
    * Document methods.
    * Use Numpy for optimizations. 

"""
import sys
import time
import math

class BruteForce:
    # Algorithm based on brute force
    def __init__(self, N):
        self.N = N
    
    def run(self):
        '''
        Basicamente por cada numero verifica si es primo. 
        Para ello partimos de su  definicion:
        Un numero primo es aquel que es divisible exclusivamente por 
        si mismo.
        Entonces si la cantidad de divisores es menor a 2 se trata de
        un numero primo.
        Consideramos que el 1 no es primo por definicion.  
        '''
        prime_numbers = []
        for i in range(2, self.N + 1):
            counter = 0
            for k in range(1, i):
                if i%k == 0:
                    counter += 1
            if counter < 2:
                prime_numbers.append(i)
        return prime_numbers

class Erastotenes:
    # Algorithm based on erastotenes
    def __init__(self, N):
        self.N = N
    
    def run(self):
        '''
        Consideramos que el 1 no es primo por definicion. 
        '''
        sqrt_n =  int(math.sqrt(self.N)) + 1
        tam_array = int(self.N / 2)
        
        array_numbers = []
        for i in range(tam_array):
            array_numbers.append(0)
        
        prime_numbers = [2]
        for i in range(3, sqrt_n, 2):
            if array_numbers[int(i/2)]:
                continue
            j = int(i*i/2)
            for x in range(j, tam_array, i):
                array_numbers[x] = 1
        
        for i in range(1, tam_array):
            if not array_numbers[i]:
                prime_numbers.append(i*2 + 1)
        
        return prime_numbers
    
if __name__ == '__main__':
    start = time.time()
    if len(sys.argv) < 3: 
        print("Error con los argumentos pasados. Revise documentacion")
        exit(1)
    
    N = int(sys.argv[1])
    algorithm = sys.argv[2]
    
    if algorithm == "F" or algorithm == "f":
        processor = BruteForce(N)
    elif algorithm == "E" or algorithm == "e":
        processor = Erastotenes(N)
    else:
        print("Error con los argumentos pasados. Revise documentacion")
        exit(1)
    
    prime_numbers = processor.run()
    #print("Numeros primos: ", prime_numbers)
    print("Tiempo de ejecucion: ", time.time() - start, " segundos.")


# authorship information
__author__ = "Federico Manuel Gomez Peter, Martin Cruz Garcia, and "
__author__ += "Etcheverry Lucas"
__copyright__ = ""
__credits__ = ["Federico Manuel Gomez Peter", "Martin Cruz Garcia", 
                "Etcheverry Lucas"]
__license__ = "GNU"
__version__ = "1.0"
__maintainer__ = ""
__email__ = "lucas.etcheverry28@gmail.com"
__status__ = "Development"
