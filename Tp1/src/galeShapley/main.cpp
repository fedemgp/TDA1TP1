

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <queue>
#include <map>

#include <Exception.h>
#include <Chronometer.h>
#include "main.h"
#include "RankedItem.h"

typedef std::vector<std::vector<unsigned int>> matrix_t;

void initialize(matrix_t &bands, matrix_t &concerts,
				std::queue<unsigned int> &queue, const unsigned int N,
				const unsigned int M);
void GS(const unsigned int N, const unsigned int M, const unsigned int X,
		const unsigned int Y, const matrix_t &concerts_matrix,
		const matrix_t &bands_matrix, std::queue<unsigned int> &queue,
        std::vector<std::priority_queue<RankedItem>> &tours_list);

void printResult(std::vector<std::priority_queue<RankedItem>> &tours_list);

int main(int argc, const char *argv[]) {
	try {
		if(argc != ARGS_QUANTITY) throw Exception(ARGS_QUANTITY_ERROR_MSG,
												  ARGS_QUANTITY);
		Chronometer chrono;
		unsigned int N(0), M(0), X(0), Y(0);

		std::istringstream iss(argv[1]);
		iss >> N;
		iss = std::istringstream(argv[2]);
		iss >> M;
		iss = std::istringstream(argv[3]);
		iss >> X;
		iss = std::istringstream(argv[4]);
		iss >> Y;

		// indexa la prioridad y guardo la banda
		matrix_t concerts_matrix;
		// reservo espacio para contener exactamente N filas
		concerts_matrix.reserve(N);
		// indexa el concierto y guardo la prioridad
		matrix_t bands_matrix;
		// reservo espacio para contener exactamente M filas
		bands_matrix.reserve(M);
		// conciertos con al menos un cupo libre
		std::queue<unsigned int> available_concerts;
		// para cada banda, su lista de conciertos agendados
		std::vector<std::priority_queue<RankedItem>> tours_list(M);

		initialize(bands_matrix, concerts_matrix, available_concerts, N, M);
#ifdef DEBUG
		std::cout << "Tiempo para inicializar variables: " << chrono
				  << std::endl;
#endif
		GS(N, M, X, Y, concerts_matrix, bands_matrix, available_concerts,
		   tours_list);
#ifdef DEBUG
		std::cout << "Tiempo de cálculo: " << chrono << std::endl;
		printResult(tours_list);
#endif
#ifdef PRINT_ONLY_TIME
        std::cout << chrono << std::endl;
#endif
	} catch (std::exception &e) {
		std::cerr << e.what() << std::endl;
		return 1;
	} catch (...) {
		std::cerr << UNKNOWN_ERROR_MSG << std::endl;
		return 1;
	}
	return 0;
}

void printResult(std::vector<std::priority_queue<RankedItem>> &tours_list) {
	int i = 0;
	for (auto &v : tours_list) {
		std::cout << "Tour de banda " << i++ << ":" << std::endl;

        while (!v.empty()) {
            std::cout << "\tConcierto "<<  v.top().getValue() << std::endl;
            v.pop();
        }
	}
}

void GS(const unsigned int N, const unsigned int M, const unsigned int X,
		const unsigned int Y, const matrix_t &concerts_matrix,
		const matrix_t &bands_matrix,
		std::queue<unsigned int> &available_concerts,
        std::vector<std::priority_queue<RankedItem>> &tours_list) {
	// cantidad de conciertos que ya se ofrecieron a todas las bandas
	// o que completaron sus cupos
	unsigned int closed_concerts(0);
	// para cada concierto, la siguiente banda a la que todavia no se
	// ofrecio en su lista
	std::vector<unsigned int> next_band(N, 0);
	// para cada concierto, la cantidad de lugares libres que le quedan
	std::vector<unsigned int> free_places(N, X);

	while(closed_concerts < N) {
		// Concierto actual
		unsigned int c = available_concerts.front();
		// banda actual
		unsigned int b = concerts_matrix[c][next_band[c]];
		unsigned int actual_priority = bands_matrix[b][c];
		auto &tour = tours_list[b];
		if(tour.size() < Y) {
			tour.emplace(actual_priority, c);
			free_places[c]--;
			next_band[c]++;
			if(free_places[c] > 0) {
				if(next_band[c] >= M) {
					closed_concerts++;
                    available_concerts.pop();
                }
			} else {
				closed_concerts++;
                available_concerts.pop();
            }
		} else {
			// obtiene la minima prioridad
			auto &it = tour.top();
			unsigned int min_priority = it.getKey();
			if(min_priority > actual_priority) {
				unsigned int last_c = it.getValue();
				tour.pop();

				tour.emplace(actual_priority, c);
				free_places[c]--;
				free_places[last_c]++;
				next_band[c]++;
				/*
				 * Si la banda tenía lugares libres antes significa que ya
				 * estaba en available_concerts, entonces solo hay que volverla
				 * a agregar en el caso de que su free_places sea 1.
				 */
				if (next_band[last_c] < M && free_places[last_c] == 1) {
					available_concerts.push(last_c);
					closed_concerts--;
				}

				if(free_places[c] > 0) {
					if (next_band[c] >= M) {
						closed_concerts++;
                        available_concerts.pop();
                    }
				} else {
					closed_concerts++;
				}
			} else {
				next_band[c]++;
				if(next_band[c] == M) {
					closed_concerts++;
					available_concerts.pop();
				}
			}
		}
	}
}

void initialize(matrix_t &bands, matrix_t &concerts,
				std::queue<unsigned int> &queue, const unsigned int N,
				const unsigned int M) {
	for (unsigned int i = 0; i < N; i++) {
		queue.push(i);
	}

	// Inicializo la matriz de conciertos
	for (unsigned int i = 0; i < N; i++) {
		std::vector<unsigned int> v(M);
		std::ostringstream file_name("");
		file_name << CONCERT_FILE_PREFIX << i;
		std::ifstream is(file_name.str());
		for (unsigned int j = 0; j < M; j++) {
			unsigned int band;
			is >> band;
			v[j] = band;
		}
		concerts.emplace_back(std::move(v));
	}

	for (unsigned int i = 0; i < M; i++) {
		// para indexar por concierto en bands_matrix
		unsigned int indx(0);
		std::vector<unsigned int> v(N);
		std::ostringstream file_name("");
		file_name << BAND_FILE_PREFIX << i;
		std::ifstream is(file_name.str());
		for (unsigned int j = 0; j < N; j++) {
			is >> indx;
			v[indx] = j;
		}
		bands.emplace_back(std::move(v));
	}
}
