
#ifndef _MAIN_H_
#define _MAIN_H_

#define ARGS_QUANTITY 5
#define ARGS_QUANTITY_ERROR_MSG "Cantidad inválida de argumentos. "\
    "Deben pasarse %i argumentos. Ejemplo:\n ./galeShapley N M X Y, "\
    "con N M  X Y números enteros.\n"
#define UNKNOWN_ERROR_MSG "Error desconocido en main()"
#define CONCERT_FILE_PREFIX "recital_"
#define BAND_FILE_PREFIX "banda_"

#endif  // _MAIN_H_
