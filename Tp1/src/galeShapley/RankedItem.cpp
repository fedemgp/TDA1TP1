/*
 *  Created by Federico Manuel Gomez Peter.
 *  date: 22/09/18
 */

#include "RankedItem.h"

RankedItem::RankedItem(unsigned int key, unsigned int value): key(key),
                                                              value(value) {}

bool operator<(const RankedItem &a, const RankedItem &b) {
    return a.getKey() < b.getKey();
}

bool operator>(const RankedItem &a, const RankedItem &b) {
    return a.getKey() > b.getKey();
}

unsigned int RankedItem::getValue() const {
    return this->value;
}

unsigned int RankedItem::getKey() const {
    return this->key;
}

RankedItem::RankedItem(RankedItem &&other) noexcept: key(other.key),
                                                     value(other.value) {
    other.key = 0;
    other.value = 0;
}

RankedItem& RankedItem::operator=(RankedItem &&other) noexcept {
    if (this != &other) {
        this->value = other.value;
        this->key = other.key;

        other.key = 0;
        other.value = 0;
    }
    return *this;
}
