/*
 *  Created by Federico Manuel Gomez Peter.
 *  date: 22/09/18
 */

#ifndef __RANKED_ITEM_H__
#define __RANKED_ITEM_H__

/**
 * Clase contenedora para usar en el la priority_queue.
 */
class RankedItem{
 public:
    RankedItem(unsigned int key, unsigned int value);
    ~RankedItem() = default;
    RankedItem(RankedItem&& other) noexcept;

    RankedItem& operator=(RankedItem &&other) noexcept;
    unsigned int getValue() const;
    unsigned int getKey() const;

 private:
    // Deshabilito las copias
    RankedItem(const RankedItem &copy) = delete;
    RankedItem& operator=(const RankedItem &copy) = delete;
    unsigned int key;
    unsigned int value;
};

bool operator<(const RankedItem &a, const RankedItem &b);
bool operator>(const RankedItem &a, const RankedItem &b);

#endif  // __RANKED_ITEM_H__
