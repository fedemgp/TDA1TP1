/*
 * Created by Federico Manuel Gomez Peter
 * on 05/09/18.
 */

#ifndef __MAIN_H__
#define __MAIN_H__

#define ARGS_QUANTITY 3
#define ARGS_QUANTITY_ERROR_MSG "Cantidad inválida de argumentos. "\
    "Deben pasarse %i argumentos. Ejemplo:\n ./fileGen N M, "\
    "con N M números enteros.\n"
#define UNKNOWN_ERROR_MSG "Error desconocido en main()"

#define BAND_FILE_PREFIX "banda_"
#define BAND_TIME_ELAPSED_MSG "Terminó de generar las bandas. Tiempo"\
    " transcurrido: "
#define RECITAL_FILE_PREFIX "recital_"
#define RECITAL_TIME_ELAPSED_MSG "Terminó de generar los conciertos. Tiempo "\
    "transcurrido: "
#endif  // __MAIN_H__
