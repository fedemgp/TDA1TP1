/*
 * Created by Federico Manuel Gomez Peter
 * on 05/09/18.
 */

#include <algorithm>
#include <vector>
#include <Exception.h>

#include "priorityList.h"

PriorityList::PriorityList(std::string &&file_path, unsigned int quantity) :
    file(file_path, std::ios_base::out), elements_quantity(quantity) {}

void PriorityList::operator()() {
    std::vector<unsigned int> vector(this->elements_quantity, 0);
    // Inserto los elementos de forma ordenada
    for (unsigned int i = 0; i < this->elements_quantity; i++) {
        vector[i] = i;
    }

    std::random_shuffle(vector.begin(), vector.end());

    for (auto i : vector) {
        this->file << i << std::endl;
        if (this->file.fail()) {
            throw Exception(WRITE_ERROR_MSG, i);
        }
    }
}
