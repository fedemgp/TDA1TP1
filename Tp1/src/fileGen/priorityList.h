/*
 * Created by Federico Manuel Gomez Peter
 * on 05/09/18.
 */

#ifndef __PRIORITY_LIST_H__
#define __PRIORITY_LIST_H__


#define WRITE_ERROR_MSG "Falló la escritura del archivo al querer insertar %i\n"

#include <fstream>
#include <string>
/**
 * Functor que se construye creando un archivo definido en file_path, y que
 * tendrá quantity elementos en el. Cada uno de estos elementos representa
 * la prioridad del conjunto al cual se prioriza.
 */
class PriorityList {
 private:
    std::fstream file;
    unsigned int elements_quantity{0};

 public:
    PriorityList(std::string &&file_path, unsigned int quantity);
    void operator()();
};


#endif  // __PRIORITY_LIST_H__
