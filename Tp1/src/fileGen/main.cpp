/*
 * Created by Federico Manuel Gomez Peter
 * on 05/09/18.
 */

#include <iostream>
#include <sstream>

#include <Chronometer.h>
#include <Exception.h>
#include "main.h"
#include "priorityList.h"

int main(int argc, const char *argv[]) {
    try {
        if (argc != ARGS_QUANTITY) {
            throw Exception(ARGS_QUANTITY_ERROR_MSG, ARGS_QUANTITY);
        }
        std::srand(std::time(nullptr));
        Chronometer chrono;
        // Cantidad de recitales
        unsigned int N(0);
        // Cantidad de bandas
        unsigned int M(0);
        std::istringstream iss(argv[1]);
        iss >> N;
        iss = std::istringstream(argv[2]);
        iss >> M;

        for (unsigned int i = 0; i < M; i++) {
            std::ostringstream oss;
            oss << BAND_FILE_PREFIX << i;
            PriorityList list(oss.str(), N);
            list();
        }

        std::cout << BAND_TIME_ELAPSED_MSG
        << chrono << std::endl;
        for (unsigned int i = 0; i < N; i++) {
            std::ostringstream oss;
            oss << RECITAL_FILE_PREFIX << i;
            PriorityList list(oss.str(), M);
            list();
        }
        std::cout << RECITAL_TIME_ELAPSED_MSG
                  << chrono << std::endl;
    } catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
        return 1;
    } catch (...) {
        std::cerr << UNKNOWN_ERROR_MSG << std::endl;
        return 1;
    }
    return 0;
}
