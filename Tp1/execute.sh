#!/bin/bash
python ./cpplint.py --filter=`cat filter_options` `find -regextype posix-egrep -regex './(libs|src)/.*\.(h|hpp|c|cpp)'`
