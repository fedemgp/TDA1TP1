# Trabajo práctico N° 2
- Devesa, Leandro Gonzalo
- Etcheverry, Lucas
- García, Martín Cruz
- Gómez Peter, Federico Manuel

# Parte 1: tarjetas coleccionables

Los códigos están escritos en Python3. Para ejecutarlos se les debe pasar los
siguientes argumentos:

## isCannonical.py
- El archivo con los tamaños de los sobres (deben estar ordenados ascendentemente).

## findMinimumPacketSet.py
- El archivo con los tamaños de los sobres (deben estar ordenados ascendentemente
y separados por lineas).
- La cantidad de cartas que se quieren comprar.
- El tipo de algoritmo a utilizar (G para usar Greedy, D para usar un algorítmo de
programación dinámica)

# Parte 2: Redes de Subterráneos.

Los códigos están escritos en Python3. Para ejecutarlos se les debe pasar los
siguientes argumentos:

- El path del archivo con la información de las estaciones.
- El path del archivo con la información de las combinaciones.
- El path del archivo con la información de las conexiones entre estaciones de la misma linea.
- La capacidad del vagón.
- La cantidad de vagones.

Como ejemplo se provee la carpeta input con los archivos necesarios.

El archivo con la información de las estaciones posee el siguiente formato: cada línea del archivo representa una estación mediante 3 datos separados por una coma: Nombre de la línea, Nombre de la estación y la diferencia entre la cantidad de gente que entra y sale de la estación.

El archivo con información de las combinaciones posee el siguiente formato: cada línea del archivo representa una combinación entre dos líneas de subte mediante 4 datos separados por una coma: Nombre de la línea, nombre de la estación, nombre de la línea, nombre de la estación.

El archivo con información de las conexiones posee el siguiente formato: cada línea del archivo representa una conexión mediante 4 datos separados por una coma: nombre de la línea, nombre de estación origen, nombre de la estación destino y capacidad del tramo.  


