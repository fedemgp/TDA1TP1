class Edge():

  def __init__(self, s, t, c):
    self.source = s
    self.target = t
    self.capacity = c

class Graph():

  def  __init__(self):
    self.adj = {}
    self.flow = {}

  def getAdj(self):
    return self.adj

  def getFlow(self):
    return self.flow

  def addVertex(self, vertex):
    self.adj[vertex] = []

  def getEdges(self, v):
    return self.adj[v]

  def addEdge(self, u, v, w = 0):
    if u == v:
      raise ValueError("u == v")
    edge = Edge(u, v, w)
    redge = Edge(v, u, w)
    edge.redge = redge
    redge.redge = edge
    self.adj[u].append(edge)
    self.adj[v].append(redge)
    # Intialize all flows to zero
    self.flow[edge] = 0
    self.flow[redge] = 0
