class FlowNetwork():

  def __init__(self, graph):
    self.adj = graph.getAdj()
    self.flow = graph.getFlow()
  
  def getEdges(self, v):
    return self.adj[v]

  def findPath(self, source, target, path, edgesVisited):
    if source == target:
      return path
    edgesVisited.append(source)
    edges = self.getEdges(source)
    for edge in edges:
      if edge.target in edgesVisited:
        continue
      residual = edge.capacity - self.flow[edge]
      if residual > 0 and not (edge, residual) in path:
        result = self.findPath(edge.target, target, path + [(edge, residual)], edgesVisited)
        if result != None:
          return result

  def maxFlow(self):
    source = "s"
    target = "t"
    path = self.findPath(source, target, [], [])
    while path != None:
      flow = min(res for edge, res in path)
      for edge, res in path:
        self.flow[edge] += flow
        self.flow[edge.redge] -= flow
      path = self.findPath(source, target, [], [])
    return sum(self.flow[edge] for edge in self.getEdges(source))