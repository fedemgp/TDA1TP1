import math
from collections import defaultdict
from Graph import Graph

class SubwayNetwork:

	NET_TRANSIT = "net_transit"

	def __init__(self, stations_file, combinations_file, connections_file, people_per_wagon):

		self.head_stations = []
		self.tail_stations = []
		self.stations = defaultdict(dict)
		self.combinations = defaultdict(dict)
		self.graph = Graph()

		self.process_stations(stations_file)
		self.process_combinations(combinations_file)
		self.init_graph()
		self.people_per_wagon = people_per_wagon
		self.process_connections(connections_file, people_per_wagon)
		self.extend_graph()

	def get_graph(self):
		return self.graph

	def extend_graph(self):
		self.graph.addVertex("s")
		self.graph.addVertex("t")

		for key, value in self.stations.items():
			if value[self.NET_TRANSIT] > 0:
				self.graph.addEdge("s", key, self.stations[key][self.NET_TRANSIT])

			if value[self.NET_TRANSIT] < 0:
				self.graph.addEdge(key, "t", -self.stations[key][self.NET_TRANSIT])

	def process_connections(self, path, people_per_wagon):
		with open(path, 'r') as file:
			lines = file.read().splitlines()
			for l in lines:
				line, from_station, to_station, maxw = l.split(",")

				from_station_id = self.get_station_id(line, from_station)
				to_station_id = self.get_station_id(line, to_station)

				# Verify if stations are in a combination
				if from_station_id in self.combinations:
					from_station_id = self.combinations.get(from_station_id)
				if to_station_id in self.combinations:
					to_station_id = self.combinations.get(to_station_id)

				self.graph.addEdge(from_station_id, to_station_id, int(maxw)*people_per_wagon)

	def init_graph(self):
		for station_id, v in self.stations.items():
			self.graph.addVertex(station_id)

	def process_combinations(self, path):
		with open(path, 'r') as file:
			lines = file.read().splitlines()
			for l in lines:
				line_a, station_a, line_b, station_b = l.split(",")

				station_a_id = self.get_station_id(line_a, station_a)
				station_b_id = self.get_station_id(line_b, station_b)

				# Verify if stations are already in a combination
				if station_a_id in self.combinations:
					station_a_id = self.combinations.get(station_a_id)
				if station_b_id in self.combinations:
					station_b_id = self.combinations.get(station_b_id)
				# Generate a new station id and sum their transits
				combination_id = self.get_combination_id(station_a_id, station_b_id)
				transit = self.stations[station_a_id][self.NET_TRANSIT] + self.stations[station_b_id][self.NET_TRANSIT]
				self.stations[combination_id] = { self.NET_TRANSIT: int(transit) }
				# Link stations with their combinations
				aIds = station_a_id.split("|")
				bIds = station_b_id.split("|")
				for i in aIds:
					self.combinations[i] = combination_id
				for i in bIds:
					self.combinations[i] = combination_id
				# Delete station ids that are combinated
				self.stations.pop(station_a_id)
				self.stations.pop(station_b_id)

	def process_stations(self, path):
		with open(path, 'r') as file:
			previous_line = ""
			previous_station = ""
			lines = file.read().splitlines()
			for l in lines:
				line, station, transit = l.split(",")

				station_id = self.get_station_id(line, station)

				if line != previous_line:
					if previous_station != "":
						self.tail_stations.append(previous_station)
					self.head_stations.append(station_id)
					previous_line = line

				previous_station = station_id
				self.stations[station_id] = { self.NET_TRANSIT: int(transit) }
			self.tail_stations.append(previous_station)

	def get_station_id(self, line_name, stop_name):
		return line_name + "-" + stop_name.replace(" ", "_")

	def get_combination_id(self, station_a, station_b):
		return station_a + "|" + station_b

	def get_demand(self):
		demand = 0
		for e in self.graph.getEdges("s"):
			demand += e.capacity
		return demand

	def get_wagons_needed(self):
		quantity = 0
		for e in self.graph.getEdges("s"):
			quantity += math.ceil(float(e.capacity) / float(self.people_per_wagon))
		return quantity
