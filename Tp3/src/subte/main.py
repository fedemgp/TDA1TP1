#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from SubwayNetwork import SubwayNetwork
from FlowNetwork import FlowNetwork


def verification():
    if sys.version_info[0] < 3:
        print("ERROR! El programa se debe ejecutar en Python 3")
        exit(1)
    if len(sys.argv) != 6:
        msg = "Error!. Se debe proporcionar 5 argumentos: \n"
        msg += "* El path del archivo con la información de las estaciones\n"
        msg += "* El path del archivo con la información de las combinaciones\n"
        msg += "* El path del archivo con la información de las conexiones entre estaciones de la misma linea\n"
        msg += "* La capacidad del vagón\n"
        msg += "* La cantidad de vagones."
        print(msg)
        exit(1)


def main():
    verification()
    stations_file_name = sys.argv[1]
    combinations_file_name = sys.argv[2]
    connections_file_name = sys.argv[3]
    people_per_wagon = int(sys.argv[4])
    total_wagons = int(sys.argv[5])

    sn = SubwayNetwork(stations_file_name, combinations_file_name, connections_file_name, people_per_wagon)

    fn = FlowNetwork(sn.get_graph())

    demand = sn.get_demand()
    maxFlow = fn.maxFlow()
    wagons = sn.get_wagons_needed()
    if demand == maxFlow and wagons <= total_wagons:
        print("Es factible dar un buen servicio.")
    else:
        print("No es factible dar un buen servicio.")
    return 0


if __name__ == "__main__":
    main()
