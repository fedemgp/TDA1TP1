#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from Greedy import *
from DynamicProgramming import *

'''
    Algoritmo que calcula la cantidad mínima de sobres que debe devolver
    para una cantidad x de cartas que se quiera obtener.
    
    Pre:    el archivo de tamaños de los sobres debe estar ordenado 
            ascendentemente
'''
def verification():
    if sys.version_info[0] < 3:
        print("ERROR! El programa se debe ejecutar en Python 3")
        exit(1)
    if len(sys.argv) < 4:
        msg = "Error!. Se debe proporcionar un argumento: \n"
        msg += "* El nombre del archivo con la información de los sobres\n"
        msg += "* La cantidad de cartas a retornar\n"
        msg += "* El carácter G o D si se quiere resolverlo mediante "
        msg += "* Greedy o por Programación Dinámica"
        print(msg)
        exit(1)

    if sys.argv[3].upper() not in ["G", "D"]:
        msg += "Error! se debe proporcionar un algorítmo válido: \n"
        msg += "* G si se queire resolver el problema mediante Greedy.\n"
        msg += "* D si se queire resolver el problema mediante Programación "
        msg += "Dinámica"
        print(msg)
        exit(1)

def read_file():
    filePath = sys.argv[1]
    packetSize = []
    with open(filePath, "rt") as file_data:
        for line in file_data:
            packetSize.append(int(line))
    return packetSize

def getClassManager(packetSet):
    algorithmType = sys.argv[3].upper()
    if algorithmType == "G":
        algorithm = Greedy(packetSet)
    else:
        algorithm = DynamicProgramming(packetSet)
    return algorithm

def main():
    verification()
    packetsSet = read_file()
    algorithm = getClassManager(packetsSet)
    result, totalPackets = algorithm.compute(int(sys.argv[2]))
    print("El resultado es :\n")
    for q, size in result:
        msg = "\t* "
        msg += str(q)
        if q == 1:
            msg += " sobre de "
            msg += str(size)
        else:
            msg += " sobres de "
            msg += str(size)
        if size == 1:
            msg += " carta.\n"
        else:
            msg += " cartas.\n"
        print(msg)

if __name__ == "__main__":
    main()
