#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from Greedy import *

'''
    Algoritmo que determina si un set de tamaños de paquetes es un set 
    canónico o no. Si el set es canónico, entonces existe una solución 
    óptima mediante el algoritmo de Greedy.
    
    Pre:    el archivo de tamaños de los sobres debe estar ordenado 
            ascendentemente
'''
def verification():
    if sys.version_info[0] < 3:
        print("ERROR! El programa se debe ejecutar en Python 3")
        exit(1)
    if len(sys.argv) < 2:
        msg = "Error!. Se debe proporcionar un argumento: \n"
        msg += "* El nombre del archivo con la información de los sobres\n"
        print(msg)
        exit(1)

def readFile():
    filePath = sys.argv[1]
    packetSize = []
    with open(filePath, "rt") as fileData:
        for line in fileData:
            packetSize.append(int(line))
    return packetSize
'''
    Para que la base sea canónica debe cumplirse la siguiente inecuación
        G(x) >= G(x - c) + 1
    Para todo valor x que esté dentro del rango:
        c[3] + 1 < x < c[m] + c[m-1]
    Con c[3], c[m], c[m-1] representan el tamaño del tercer, último
    y ante último sobre del set, que se encuentra ordenado de forma
    ascendente.
'''
def isCannonicalSet(sizeSet):
    l = len(sizeSet)
    lowerBound = sizeSet[2] + 1
    upperBound = sizeSet[l - 1] + sizeSet[l - 2]
    computation = Greedy(sizeSet)
    for x in range(lowerBound, upperBound):
        for c in sizeSet:
            if c > x:
                continue
            s1, t1 = computation.compute(x)
            s2, t2 = computation.compute(x - c)
            if t1 > t2 + 1:
                return False, s1, x
    return True, [], 0

def main():
    verification()
    packetsSize = readFile()
    '''
        Una base con 1 o 2 elementos es trivialmente una base canónica, si
        y solo si c1 = 1
    '''
    if len(packetsSize) < 3:
        if packetsSize[0] == 1:
            print(packetsSize, " es una base canónica")
        exit(0)
    result, counterexample, quantity = isCannonicalSet(packetsSize)
    if result:
        print(packetsSize, " es una base canónica")
    else:
        msg = " NO es una base canónica.\n"
        msg += "Un contraejemplo se obtiene al "
        msg += "obtener " + str(quantity) + " cartas.\n"
        msg += "El resultado da: \n"
        print(packetsSize, msg)
        for q, size in counterexample:
            msg = "\t* "
            msg += str(q)
            if q == 1:
                msg += " sobre de "
                msg += str(size)
            else:
                msg += " sobres de "
                msg += str(size)
            if size == 1:
                msg += " carta.\n"
            else:
                msg += " cartas.\n"
            print(msg)


main()
