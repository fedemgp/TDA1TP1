#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

class DynamicProgramming:
    def __init__(self, packets):
        self.packets = packets.copy()

    def compute(self, V):
        '''
        Arreglo que albergará el tamaño del paquete óptimo para dar una
        cantidad i de cartas, siendo este i el indice del vector.
        '''
        minPacketSize = [0] * (V + 1)
        '''
        Arreglo auxiliar para determinar cual es la cantidad de paquetes 
        óptimo a utilizar para entregar i  cartas (con i el indice del vector)
        '''
        minPacketQuantity = [0] * (V + 1)
        return self.getMinPackets(V, minPacketQuantity, minPacketSize)

    def getMinPackets(self, cardsQuantity, minPacketQuantity, minPacketSize):
        for amount in range(cardsQuantity + 1):
            cardsCount = amount
            newPacket = 1
            for packetSize in self.packets:
                if packetSize > amount:
                    continue
                if minPacketQuantity[amount - packetSize] + 1 < cardsCount:
                    cardsCount = minPacketQuantity[amount - packetSize] + 1
                    newPacket = packetSize
            minPacketQuantity[amount] = cardsCount
            minPacketSize[amount] = newPacket
        return self.getPacketArray(minPacketSize, cardsQuantity)

    def getPacketArray(self, packetsUsed, amount):
        packetsAmountPerSize = {}
        totalPackets = 0
        while amount > 0:
            size = packetsUsed[amount]
            if (size not in packetsAmountPerSize):
                packetsAmountPerSize[size] = 1
            else:
                packetsAmountPerSize[size] += 1
            totalPackets += 1
            amount -= size
        ret = []
        '''
        Esta conversión la debo realizar para que se adapte al algorítmo de 
        greedy
        '''
        for key in packetsAmountPerSize:
            ret.append((packetsAmountPerSize[key], key))
        return ret, totalPackets
