class Greedy:
    def __init__(self, packets):
        self.packets = packets.copy()
        self.packets.sort(reverse=True)

    def compute(self, x):
        result = []
        totalPackets = 0
        for size in self.packets:
            quantity = x // size
            if quantity > 0:
                x -= quantity * size
                result.append((quantity, size))
                totalPackets += quantity
        return result, totalPackets
