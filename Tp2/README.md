# Trabajo práctico N° 2
- Etcheverry, Lucas
- García, Martín Cruz
- Gómez Peter, Federico Manuel
- Devesa, Leandro Gonzalo

## Casos de prueba
En la carpeta `src/test` hay un archivo `points.txt` con un caso de prueba con 
unos puntos que armé manualmente.

El Convex Hull de ese archivo debería ser:
```
    [(p1, p3), (p3, p2), (p2, p9), (p9, p5), (p5, p1)]
```
Donde `(pi, pj)` es el segmento conformado por el punto definido en la linea i y 
la linea j del archivo (empezando por la linea 1) 

### Generador
Para generar mas archivos de entrada, se debe correr el programa 
`src/test/points_generator.py`. Para invocar este script, se le debe pasar 6 
argumentos:

- El nombre del archivo destino donde se escribirán los puntos
- El límite inferior en x donde se quieran crear los puntos
- El límite superior de x.
- El límite inferior de y.
- El límite superior de y.
- La cantidad de puntos a crear (un número entero mayor a dos)

Con el archivo generado, no se necesita mas que correr el script `camino_seguro`
pasandole por parámetros el archivo a leer y luego el modo de ejecución:

- `F` para utilizar el algorítmo de fuerza bruta.
- `G` para utilizar Graham Scan
- `D` para realizar una busqueda por dividir y conquistar.

### Ejecución

Se debe ubicar en la carpeta Tp2/src/.
Allí se encuentra una carpeta: test, y 4 archivos: 
* camino_seguro.py
* brute_force.py
* graham_scan.py
* divide_and_conquer.py

Par ejecutar el script es necesario indicar 2 parámetros:
- El archivo con los puntos a analizar. (Hay uno de ejemplo: test/points.txt)
- El método a ejecutar: F, G o D. 

El script que se debe ejecutar es "camino_seguro.py".

Ejemplo: python3 camino_seguro.py test/points.txt F






