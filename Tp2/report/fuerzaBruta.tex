El algorítmo de fuerza bruta se basa en el hecho de que para que un par de puntos pertenezca al \emph{convex hull} es condición necesaria y suficiente que la recta que une a estos sea tal que deje al resto de los puntos en el mismo semiplano. 

\subsubsection{Algorítmo}
\label{sec:algoFB}
\newcommand{\Not}[1]{\textbf{!}#1}
\newcommand{\And}{\textbf{and}\,\,}
\newcommand{\Or}{\textbf{or}\,\,}

Partiendo de la premisa anterior, el algorítmo se puede resumir en que se van tomando de a pares de puntos, y si este par es tal que deja a todos los demás puntos en un mismo semiplano, entonces es parte de la envoltura. Pero, ¿Cómo se puede determinar que todos los puntos están en el mismo semiplano? Para esto, se calcula la ecuación de la recta que pasa por ambos puntos:

\begin{equation}
	y - m \cdot x - b = 0
	\label{eq:recta}
\end{equation}

Donde $m$ es la pendiente de la recta:

\begin{equation}
	m = \frac{y_1 - y_2}{x_1 - x_2}
	\label{eq:pendiente}
\end{equation}

Y $b$ es el punto donde la recta corta al eje de coordenadas:

\begin{equation}
	b = y_1 - m \cdot x_1
	\label{eq:oordenadaAlOrigen}
\end{equation}

Una vez obtenida la ecuación de la recta, se toman todos los puntos $(x_i, y_i)$ y se los evalúa en la ecuación \ref{eq:recta}. Los resultados posibles que puede dar este reemplazo es un número negativo (se encuentra en el semiplano izquierdo), un número positivo (se encuentra en el semiplano derecho) o cero (es un punto que cumple la ecuación de la recta). Por lo tanto con la ecuación de la recta se puede ver si todos los puntos pertenecen al mismo semiplano, y así determinar si el segmento pertenece al \emph{convex hull}.

\subsubsection{Pseudocódigo}
\label{sec:pseudoFB}

\begin{algorithm}[H]
\caption{Calcular la envoltura convexa por fuerza bruta}
\label{code:algoritmo}
\begin{algorithmic}[1]
\Require $points = [[x1,y1], ..., [xn,yn]]$ \Comment{Arreglo de puntos seguros}

\State convex\_segments := [] \Comment{Segmentos de la envoltura}
\State convex\_hull := [points[0]] \Comment{Arreglo de puntos que conforman la envoltura}
\State candidates := []
\State
\For{$i = 0$, $i < points.size()$, $i++$)}
\For{($j = 0$, $j < points.size()$, $j++$)}
\State \Comment{No tiene sentido analizar un segmento compuesto por el mismo punto}
\If{points[$i$] == points[$j$]}
\State continue
\EndIf
\State \Comment{No vale la pena analizar un segmento de la envoltura ya analizado}
\If{convex\_segments.contains(segment(points[$i$], points[$j$]))}
\State continue
\EndIf
\If{points[$i$][0] - points[$j$][0]}  \Comment{Recta paralela al eje y}
\State [left, right] := compute\_with\_inifinite\_slope(i, j)
\Else
\State [left, right] := compute\_with\_finite\_slope(i, j)
\EndIf
\If{left == 0 \Or right == 0}
\State candidates.push([points[i], points[j] distance(points[i], points[j]) ])
\EndIf
\algstore{bkbreak}
\end{algorithmic}
\end{algorithm}

\begin{algorithm}[H]
\begin{algorithmic}[1]
\algrestore{bkbreak}
\State founded = 0
\While{founded $< 2$ \And candidates.length()$ > 0$}
\State \Comment{Obtiene los candidatos mas cercanos}
\State candidate = min\_by\_distance(candidates) 
\State candidates.remove(candidate)
\State founded ++
\If{\Not convex\_segments.contains([candidate[0], candidate[1]])}
\State convex\_segments.push([candidate[0], candidate[1]])
\EndIf
\If{\Not convex\_hull.contains(candidate[1])}
\State convex\_hull.push(candidate[1])
\EndIf
\EndWhile
\State candidates := []
\EndFor
\EndFor
\State \Return convex\_hull
\State
\State
\end{algorithmic}
\end{algorithm}

La función \texttt{calculate\_with\_finite\_slope} calcula si la recta dada por el par de puntos $(p_i, p_j)$ separa al plano real de forma tal que todos los puntos se encuentran en el mismo semiplano de la forma mencionada previamente. Calcula la ecuación de la recta y evalúa a todos los puntos
en esta ecuación para discriminar. Por el otro lado, la función \texttt{calculate\_with\_infinite\_slope} realiza lo mismo pero considerando que la recta es paralela al eje y.

\subsubsection{Complejidad}
\label{sec:complejidadFB}
Viendo el pseudocódigo de la sección \ref{sec:pseudoFB} se puede ver que hay doble ciclo for anidado que itera sobre todos los elementos del arreglo de entrada. Esto Inherentemente implica un orden de complejidad cuadrático.

En primer lugar, se puede observar que todas las operaciones realizadas dentro de los
ciclos son de orden constante salvo unas particulares: revisar dentro del arreglo 
la existencia de un nodo o segmento. En la implementación mas naive, buscar en un arreglo un elemento implica un orden lineal. Como estas consultas se realizan dentro de los dos ciclos, implicaría un término cúbico. También se encuentran las funciones de computos para pendientes finitas e infinitas. Estas dos recorren toda la lista de puntos nuevamente para evaluar la ecuación de la recta. Por lo tanto tiene una complejidad lineal, y el algoritmo final termina siendo cúbico.

\begin{equation}
	T(n,m) = N^3
\end{equation}

Por otro lado, en lo que a espacio se respecta, puede verse que las estructuras usadas de mayor peso son arreglos de puntos, y arreglos de segmentos. Los arreglos de puntos se puede representar como una matriz de $N x 2$, mientras que el arreglo de segmentos está dado por una matriz de dimensiones $M x 4$. Finalmente, se puede deducir que el orden de complejidad en el espacio del algoritmo es de $S(n) = N$.

\subsubsection{Implementación}
\label{sec:implementacionFB}
Para la implementación se utilizó en lenguaje Python, respetando el polimorfismo que debe existir entre los tres algorítmos. El código puede verse a continuación. 

\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}
\lstset{ 
  basicstyle=\footnotesize,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  frame=single,	                   % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  language=Python,                 % the language of the code
  numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=1,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{mymauve},     % string literal style
  tabsize=2,	                   % sets default tabsize to 2 spaces
  title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}
\begin{lstlisting}
from algorithm import ConvexHullAlgorithm

class BruteForce(ConvexHullAlgorithm):
    def __init__(self, array_points):
        super().__init__(array_points)

    def compute_with_finite_slope(self, pi, pj):
        m = (pi[1] - pj[1]) / (pi[0] - pj[0])
        # Donde corta al eje de coordenadas
        b = m * pi[0] - pi[1]

        leftside_counter = 0
        rightside_counter = 0
        for pw in self.array_points:
            if pw == pi or pw == pj:
                continue

            if (-m * pw[0] + pw[1] + b) > 0:
                rightside_counter += 1
                if leftside_counter > 0:
                    # Si hay puntos tanto a la izquierda como a la
                    # derecha (pi, pj) no es un segmento que pertenece
                    # al convex Hull
                    break
            elif (-m * pw[0] + pw[1] + b) < 0:
                leftside_counter += 1
                if rightside_counter > 0:
                    break
        return leftside_counter, rightside_counter

    def compute_convex_wrap(self):
        '''
        caso trivial en el que el arreglo de puntos tiene tamano 2.
        Se devuelve el mismo arreglo que ya es un "convex hull"
        '''
        if len(self.array_points) == 2:
            return self.array_points

        # Arreglo auxiliar que guarda los segmentos del convex hull.
        convex_segments = []
        convex_hull = [self.first_point]
        candidates = []

        for pi in self.array_points:
            for pj in self.array_points:
                if pi == pj:
                    continue
                # No lo inserto dos veces al mismo segmento
                if (pi, pj) in convex_segments or (pj, pi) in convex_segments:
                    continue

                # Pendiente de la recta
                if pi[0] - pj[0] != 0:
                    left, right = self.compute_with_finite_slope(pi, pj)
                else:
                    left, right = self.compute_with_infinite_slope(pi, pj)

                if left == 0 or right == 0:
                    # Todos los puntos estan a la derecha o todos estan a la
                    # izquierda entonces agrego el segmento (pi, pj)
                    # a la lista de candidatos
                    candidates.append([pi, pj, self.distance(pi, pj)])

                founded = 0
                while founded < 2 and len(candidates) > 0:
                    # Obtengo el condidato de a ser parte del convex hull que posea
                    # la menor distancia euclidea
                    pj, pw, d = min(candidates, key = lambda t: t[2])
                    candidates.remove([pj, pw, d])
                    founded += 1
                    if (pw, pj) not in convex_segments or (pj, pw) not in convex_segments:
                        convex_segments.append((pi, pj))
                    if pj not in convex_hull:
                        convex_hull.append(pj)
                candidates = []
        return self.sort_points(convex_hull)

    # Devuelve la norma cuadrado del par de puntos
    def distance(self, pi, pj):
        return (pi[0] - pj[0]) ** 2 + (pi[1] - pj[1]) ** 2

    def compute_with_infinite_slope(self, pi, pj):
        leftside_counter = 0
        rightside_counter = 0
        for pw in self.array_points:
            if pw == pi or pw == pj:
                continue
            if pw[0] < pi[0]:
                leftside_counter += 1
                if rightside_counter > 0:
                    break
            elif pw[0] > pi[0]:
                rightside_counter += 1
                if leftside_counter > 0:
                    break
        return leftside_counter, rightside_counter
\end{lstlisting}