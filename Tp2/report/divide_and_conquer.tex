\subsubsection{Descripción del algoritmo}

Existe más de una forma de encarar el problema de la envoltura convexa mediante una estrategia \textit{divide \& conquer}. La solución propuesta consiste en tomar dos puntos cualesquiera de la envoltura y definir la recta que cruza por ambos puntos. Una característica deseable de esta recta es que divida al conjunto de puntos a la mitad. Si bien los puntos pueden ser tomados al azar dentro de la envoltura, una elección natural es tomar los puntos más a la izquierda y más a la derecha del conjunto, como se muestra en la figura \ref{fig:divide1}. En el caso del camino del robot se conocen de antemano dos puntos de la envoltura, que son el punto inicial y final de los recorridos, por lo que se pueden usar esos.

\begin{figure}
		\centering
		\includegraphics[width=.7\linewidth]{img/divide1.png}
		\caption{Ejemplo divide \& conquer, paso 1}
		\label{fig:divide1}
\end{figure}

Una vez definida dicha recta se debe obtener el punto más distante a un lado de la recta. Este punto necesariamente pertence a la envoltura convexa y nos permite definir un triángulo junto a los dos puntos iniciales. Los puntos interiores a dicho triángulo no pertenecen a la envoltura por lo que pueden ser ignorados en las operaciones posteriores. Esto se ejmplifica en la figura \ref{fig:divide2}, donde los puntos que se sabe que no pertenecen a la envoltura son marcados en rojo.

\begin{figure}
		\centering
		\includegraphics[width=.7\linewidth]{img/divide2.png}
		\caption{Ejemplo divide \& conquer, paso 2}
		\label{fig:divide2}
\end{figure}

El próximo paso consiste en realizar la misma operación pero sobre las aristas del triangulo resultante, como se ve en las figuras \ref{fig:divide3} y \ref{fig:divide4}.

\begin{figure}
		\centering
		\includegraphics[width=.7\linewidth]{img/divide3.png}
		\caption{Ejemplo divide \& conquer, paso 3}
		\label{fig:divide3}
\end{figure}

\begin{figure}
		\centering
		\includegraphics[width=.7\linewidth]{img/divide4.png}
		\caption{Ejemplo divide \& conquer, paso 4}
		\label{fig:divide4}
\end{figure}

Así se sigue recursivamente sobre las aristas de los sucesivos triangulos que se van formando hasta que no queden puntos por fuera de la envoltura. Finalmente se aplica el mismo procedimiento para los puntos al otro lado de la recta inicial y se obtiene la envoltura completa, el resultado final para el ejemplo dado se ve en la figura \ref{fig:divide5}.

\begin{figure}
		\centering
		\includegraphics[width=.7\linewidth]{img/divide5.png}
		\caption{Ejemplo divide \& conquer, paso 5}
		\label{fig:divide5}
\end{figure}

\subsubsection{Complejidad}

En algoritmo propuesto es en cierta forma similar al algoritmo de ordenamiento \textit{quicksort}, en el sentido de que siempre se usa una recta sobre la que se ``pivotean'' los puntos sobre los que se van calculando las distancias a la recta. Así, la complejidad resulta similar a la de dicho algoritmo ya que responden a la misma ecuación de recursión:

\begin{equation}
	T(n) = T(n_1) + T(n_2) + \Theta(n)
\end{equation}

donde $n_1 + n_2 = n$.

Dicha ecuación tiene una solución conocida y en el peor caso, cuando $n_1$ y $n_2$ son muy distintos, $T(n)$ queda acotada según $O(n^2)$.

En cuanto a lo espacial, debido al tipo de implementación que se eligió, donde se genera una copia del arreglo de puntos a analizar en cada caso para poder ir quitando de la misma los puntos que van quedando dentro de las envolturas parciales, en el peor caso, que es en el que todos los puntos pertenecen a la envoltura, la complejidad resulta $O(n^2)$. 

\subsubsection{Implementación}

La implementación fue realizada en lenguje Python, al igual que las demás variantes de este problema. El código se muestra a continuación:

\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}
\lstset{ 
  basicstyle=\footnotesize,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  frame=single,	                   % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  language=Python,                 % the language of the code
  numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=1,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{mymauve},     % string literal style
  tabsize=2,	                   % sets default tabsize to 2 spaces
  title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}
\begin{lstlisting}
#!/usr/bin/env python
# -*- coding: utf-8 -*-
from algorithm import ConvexHullAlgorithm
import math

class DivideAndConquer(ConvexHullAlgorithm):
	def __init__(self, array_points):
		super().__init__(array_points)

	def lineDist(self, a, b, p):
		return abs((p[1]-a[1])*(b[0]-a[0]) - (b[1]-a[1])*(p[0]-a[0]))

	def side(self, a,b,p):
		aux = (p[1]-a[1])*(b[0]-a[0]) - (b[1]-a[1])*(p[0]-a[0])
		if aux > 0:
			return 1
		elif aux < 0:
			return -1
		else:
			return 0

	def quickHull(self, a, p1, p2, s, hull):
		aux = a[:]
		maxDist = 0
		for p_i in aux:
			dist = self.lineDist(p1,p2,p_i)
			if dist > maxDist and (self.side(p1, p2, p_i) == s):
				maxDist = dist
				max = p_i

		if maxDist == 0:
			return None

		for p_i in aux:
			if self.side(p1,p2,p_i) != s:
				aux.remove(p_i)

		hull.append(max)

		self.quickHull(aux,p1,max,1,hull)
		self.quickHull(aux,max,p2,1,hull)


	def compute_convex_wrap(self):
		left_hull = [self.first_point, self.last_point]
		right_hull = [self.first_point, self.last_point]
		self.quickHull(self.array_points, self.first_point, self.last_point, 1, left_hull)
		self.quickHull(self.array_points, self.last_point, self.first_point, 1, right_hull)

		# Junto las soluciones agregando los puntos que no existan todavia en el right_hull
		for p in left_hull:
			if p not in right_hull:
				right_hull.append(p)

		right_hull = self.sort_points(right_hull)
		return right_hull

\end{lstlisting}
