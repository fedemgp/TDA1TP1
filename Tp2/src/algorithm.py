import math
class ConvexHullAlgorithm:
    def __init__(self, array_points):
        # Con el orden que le llega del archivo
        self.array_points = array_points
        self.first_point = array_points[0]
        self.last_point = array_points[1]

    def sort_points(self, array_points):

        def compare(point):
            vec = [float(point[0]-self.first_point[0]),float(point[1]-self.first_point[1])]
            dist = math.hypot(vec[0], vec[1])
            ref = [0,1]
            norm = [vec[0]/dist,vec[1]/dist]
            dot_prod = norm[0]*ref[0] + norm[1]*ref[1]
            dot_prod_orth = ref[1]*norm[0] - ref[0]*norm[1]
            angle = math.atan2(dot_prod_orth,dot_prod)
            if angle < 0:
                return 2*math.pi+angle, 1/dist
            return angle, 1/dist

        return [array_points[0]] + sorted(array_points[1:], key=compare, reverse=True)

