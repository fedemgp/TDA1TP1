#!/usr/bin/env python
# -*- coding: utf-8 -*-

from algorithm import ConvexHullAlgorithm

"""
Created by Federico Manuel Gomez Peter
on 09/10/18.
"""
class BruteForce(ConvexHullAlgorithm):
    def __init__(self, array_points):
        super().__init__(array_points)

    def compute_with_finite_slope(self, pi, pj):
        m = (pi[1] - pj[1]) / (pi[0] - pj[0])
        # Donde corta al eje de coordenadas
        b = m * pi[0] - pi[1]

        leftside_counter = 0
        rightside_counter = 0
        for pw in self.array_points:
            if pw == pi or pw == pj:
                continue

            if (-m * pw[0] + pw[1] + b) > 0:
                rightside_counter += 1
                if leftside_counter > 0:
                    # Si hay puntos tanto a la izquierda como a la
                    # derecha (pi, pj) no es un segmento que pertenece
                    # al convex Hull
                    break
            elif (-m * pw[0] + pw[1] + b) < 0:
                leftside_counter += 1
                if rightside_counter > 0:
                    break
        return leftside_counter, rightside_counter

    def compute_convex_wrap(self):
        '''
        caso trivial en el que el arreglo de puntos tiene tamano 2.
        Se devuelve el mismo arreglo que ya es un "convex hull"
        '''
        if len(self.array_points) == 2:
            return self.array_points

        # Arreglo auxiliar que guarda los segmentos del convex hull.
        convex_segments = []
        convex_hull = [self.first_point]
        candidates = []

        for pi in self.array_points:
            for pj in self.array_points:
                if pi == pj:
                    continue
                # No lo inserto dos veces al mismo segmento
                if (pi, pj) in convex_segments or (pj, pi) in convex_segments:
                    continue

                # Pendiente de la recta
                if pi[0] - pj[0] != 0:
                    left, right = self.compute_with_finite_slope(pi, pj)
                else:
                    left, right = self.compute_with_infinite_slope(pi, pj)

                if left == 0 or right == 0:
                    # Todos los puntos estan a la derecha o todos estan a la
                    # izquierda entonces agrego el segmento (pi, pj)
                    # a la lista de candidatos
                    candidates.append([pi, pj, self.distance(pi, pj)])

                founded = 0
                while founded < 2 and len(candidates) > 0:
                    # Obtengo el condidato de a ser parte del convex hull que posea
                    # la menor distancia euclidea
                    pj, pw, d = min(candidates, key = lambda t: t[2])
                    candidates.remove([pj, pw, d])
                    founded += 1
                    if (pw, pj) not in convex_segments or (pj, pw) not in convex_segments:
                        convex_segments.append((pi, pj))
                    if pj not in convex_hull:
                        convex_hull.append(pj)
                candidates = []
        return self.sort_points(convex_hull)

    # Devuelve la norma cuadrado del par de puntos
    def distance(self, pi, pj):
        return (pi[0] - pj[0]) ** 2 + (pi[1] - pj[1]) ** 2

    def compute_with_infinite_slope(self, pi, pj):
        leftside_counter = 0
        rightside_counter = 0
        for pw in self.array_points:
            if pw == pi or pw == pj:
                continue
            if pw[0] < pi[0]:
                leftside_counter += 1
                if rightside_counter > 0:
                    break
            elif pw[0] > pi[0]:
                rightside_counter += 1
                if leftside_counter > 0:
                    break
        return leftside_counter, rightside_counter



