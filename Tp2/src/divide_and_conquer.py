#!/usr/bin/env python
# -*- coding: utf-8 -*-
from algorithm import ConvexHullAlgorithm
import math

class DivideAndConquer(ConvexHullAlgorithm):
	def __init__(self, array_points):
		super().__init__(array_points)

	def lineDist(self, a, b, p):
		return abs((p[1]-a[1])*(b[0]-a[0]) - (b[1]-a[1])*(p[0]-a[0]))

	def side(self, a,b,p):
		aux = (p[1]-a[1])*(b[0]-a[0]) - (b[1]-a[1])*(p[0]-a[0])
		if aux > 0:
			return 1
		elif aux < 0:
			return -1
		else:
			return 0

	def quickHull(self, a, p1, p2, s, hull):
		aux = a[:]
		maxDist = 0
		for p_i in aux:
			dist = self.lineDist(p1,p2,p_i)
			if dist > maxDist and (self.side(p1, p2, p_i) == s):
				maxDist = dist
				max = p_i

		if maxDist == 0:
			return None

		for p_i in aux:
			if self.side(p1,p2,p_i) != s:
				aux.remove(p_i)

		hull.append(max)

		self.quickHull(aux,p1,max,1,hull)
		self.quickHull(aux,max,p2,1,hull)


	def compute_convex_wrap(self):
		left_hull = [self.first_point, self.last_point]
		right_hull = [self.first_point, self.last_point]
		self.quickHull(self.array_points, self.first_point, self.last_point, 1, left_hull)
		self.quickHull(self.array_points, self.last_point, self.first_point, 1, right_hull)

		# Junto las soluciones agregando los puntos que no existan todavía en el right_hull
		for p in left_hull:
			if p not in right_hull:
				right_hull.append(p)

		right_hull = self.sort_points(right_hull)
		return right_hull
