import math
'''
The algorithm need an array with data points and 
it must have this structure:

+ Each element is an array with two elements: numbers representing 
the X and Y coordinates of points (integers).
+ The first element corresponds to the point of origin.
+ The second element corresponds to the destination point. 

Note that we already have the lowest point (start point). This points 
belongs to the convex hull.
'''
from algorithm import ConvexHullAlgorithm


class GrahamScan(ConvexHullAlgorithm):
    # Algorithm based on Graham Scan
    def __init__(self, array_points):
        super().__init__(array_points)

    def compute_convex_wrap(self):
        '''
        This method guides the whole computation of the algorithm based 
        on Graham Scan. 
        
        The first step is get the some point in the convex hull. But in this
        case we already have two. (see the file_points structure)
        
        The second step we sort the points according to the angle 
        formed by the first point and the rest.
        '''

        self.first_point = self.array_points[0]
        self.last_point = self.array_points[1]
        
        array_points = self.sort_points(self.array_points)
        # initialize the convex_wrap array.
        convex_wrap = [array_points[0], array_points[1]]
        for point in array_points[2:]:
            while len(convex_wrap) >= 2 and self.get_rot(convex_wrap[-2], convex_wrap[-1], point) >= 0:
                # remove the last item
                # pop(0) is O(N) as all the items must be shifted. Pop() however
                # is known to be a very fast operation as it removes an item 
                # from the end of the list, requiring no shifting.
                convex_wrap.pop()
            
            convex_wrap.append(point)
        # In case there are aligned points belonging to the envelope, 
        # the centers are discarded.
        return convex_wrap 
       
        
    @staticmethod
    def get_rot(p1, p2, p3):
        '''
        For each point we calculate if the movement from the two 
        previous ones is a "right turn", a "left turn" or are aligned.
        
        We can know it with the vector product between the 2 vectors:
        (p1, p2) y (p1, p3)
        
        (p2.y - p1.y)(p3.x - p1.x) - (p2.x - p1.x)(p3.y - p1.y)
        '''
        aux = (p2[1] - p1[1])*(p3[0] - p1[0]) - (p2[0] - p1[0])*(p3[1] - p1[1])
        # If aux == 0 aligned!
        # If aux > 0, The rotation is to the left! 
        # If aux > 0, The rotation is to the right!

        return aux 
        