import sys
from graham_scan import *
from brute_force import *
from divide_and_conquer import *
import math
'''
The script need a file with data points. 
This file must have this structure:

+ For each line two numbers representing the X and Y coordinates of 
safe points separated by a space (integers).
+ The first line corresponds to the point of origin.
+ The second line corresponds to the destination point 
'''

def verifications():
    if len(sys.argv) < 3:
        msg = "Error!. Debes proporcionar 2 parametros: \n"
        msg += "+ El archivo con los puntos a analizar.\n"
        msg += "+ El tipo de algoritmo a utilizar: F, G o D."
        print(msg)
        exit(1)
    if sys.argv[2].upper() not in ["F", "G", "D"]:
        msg = "Error!. El tipo de algoritmo a proporcionar debe ser: "
        msg += "F, G o D."
        msg += "Tu has proporcionado " + str(sys.argv[2].upper) + "."    
        print(msg)
        exit(1)

def get_class_manager(type_algorithm, array_points):
    if type_algorithm == "F":
        algorithm = BruteForce(array_points)
    elif type_algorithm == "G":
        algorithm = GrahamScan(array_points)
    elif type_algorithm == "D": 
        algorithm = DivideAndConquer(array_points)

    return algorithm

def print_output(path1, path2, length1, length2):
    '''
    Example output:
    Camino 1: Longitud [poner longitud del camino]
    Recorrido: [mostrar el x,y de cada punto iniciando desde origen y pasando en forma ordenada por los puntos seguros hasta el punto de destino]

    Camino 2: Longitud [poner longitud del camino]
    Recorrido: [mostrar el x,y de cada punto iniciando desde origen y pasando en forma ordenada por los puntos seguros hasta el punto de destino]

    Camino seleccionado: [1 o 2]
    '''
    print("Camino 1: Longitud", round(length1, 2))
    print("Recorrido: ", path1)
    print("\n")
    print("Camino 2: Longitud", round(length2, 2))
    print("Recorrido: ", path2) 
    print("\n")
    selected = [1, 2][length1 >= length2]
    print("Camino seleccionado: ", selected)
    
    
    
def get_paths_and_length(convex_wrap, first_point, last_point):
    i = 0
    for point in convex_wrap:
        i += 1
        if point == last_point:
            break
    path1 = convex_wrap[:i]
    path2 = [first_point] + (convex_wrap[i-1:])[::-1]
    
    length1 = 0
    length2 = 0
    
    for i in range(len(path1)-1):
        length1 += math.sqrt((path1[i][0] - path1[i+1][0]) ** 2 + (path1[i][1] - path1[i+1][1]) ** 2)
    for i in range(len(path2)-1):
        length2 += math.sqrt((path2[i][0] - path2[i+1][0]) ** 2 + (path2[i][1] - path2[i+1][1]) ** 2)
        
    return path1, path2, length1, length2 

def main():
    # Some checks..
    verifications()
    file_name = sys.argv[1]
    type_algorithm = sys.argv[2].upper()
    
    # read the file points
    array_points = []
    with open(file_name, "rt") as file_data:
        for line in file_data:
            x, y = line.split(" ") # X and Y are separated by a space.
            array_points.append([int(x), int(y)]) # X and y are integers
    first_point = array_points[0]
    last_point = array_points[1]
    
    # We decide wich class to use based on the type of algorithm.
    # All classes have the method 'compute_convex_wrap' implemented 
    algorithm = get_class_manager(type_algorithm, array_points)   
     
    # We run the method 'compute_convex_wrap' and 'print_output'. 
    convex_wrap = algorithm.compute_convex_wrap()
    path1, path2, length1, length2 = get_paths_and_length(convex_wrap, first_point, last_point)
    print_output(path1, path2, length1, length2)
     
     
if __name__ == "__main__":
    main()
    
