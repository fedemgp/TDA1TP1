import sys
import random
'''
    Generador de puntos al azar. Los primeros dos puntos los eligirá con
    un valor random en su componente y, y su componente x
    será fija en xinicial = x0 e xfinal = x1 (para asegurarme de que estos dos
    primeros puntos pertenecen a la envoltura convexa) Se le debe pasar por
    argumentos estos dos valores, y un tercer valor que represente la
    cantidad de puntos
'''
def verifications():
    if len(sys.argv) < 7:
        msg = "Error!. Debes proporcionar 6 parametros: \n"
        msg += "+ El archivo de salida.\n"
        msg += "+ El limite izquierdo (xinicial).\n"
        msg += "+ El límite derecho (xfinal).\n"
        msg += "+ El limite superior (yinicial).\n"
        msg += "+ El límite inferior (yfinal).\n"
        msg += "+ La cantidad de puntos a generar"
        print(msg)
        exit(1)
    if int(sys.argv[6]) < 3:
        msg = "Error!. La cantidad de puntos debe ser un numero mayor a 2: "
        print(msg)
        exit(1)
    if int(sys.argv[2]) > int(sys.argv[3]):
        print("El x mínimo debe ser menor al x máximo")
        exit(1)
    if int(sys.argv[4]) > int(sys.argv[5]):
        print("El y mínimo debe ser menor al y máximo")
        exit(1)

def initialize():
    xmin = int(sys.argv[2])
    xmax = int(sys.argv[3])
    ymin = int(sys.argv[4])
    ymax = int(sys.argv[5])
    quantity = int(sys.argv[6])

    values = []
    '''
    Punto inicial del problema (pertenece al convex hull)
    '''
    values.append([xmin, random.randint(ymin, ymax)])
    '''
    Punto final del problema (pertenece al convex hull)
    '''
    values.append([xmax, random.randint(ymin, ymax)])
    '''
    Inserto el resto de los puntos
    '''
    for i in range(2, quantity):
        values.append([random.randint(xmin, xmax), random.randint(ymin, ymax)])
    return values

def storeValues(values):
    file = open(sys.argv[1], "w")
    for point in values:
        file.write(str(point[0]) + " " + str(point[1]) + "\n")
    file.close();
    print("done")

def main():
    # Some checks..
    verifications()
    values = initialize()
    storeValues(values)


if __name__ == "__main__":
    main()

